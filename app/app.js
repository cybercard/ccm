/***
 * Тому кто будет разбираться в коде:
 * Прости, брат, но сроки страшно поджимают.
 * Начальство хочет завершить проект за неделю, а надо минимум месяц.
 * Приходиться говнокодить и индусить.
 *
 * Прикольно, когда ты всё это пишешь сам себе )))
 */

app = new (function() {
    var s = this;
    var cores = {};
    var modules = {};
    var swipes = [];

    s.url = 'http://85.143.223.130:7645/';
    s.dev = function() { return true; };
    s.device = null;

    /**
     * Работа с модулями системы
     */
    s.core = function(name, controller) {
        /**
         * Если контроллер не передан, то возвращаем ссылку на модуль
         * иначе создаём новый модуль
         */
        if(controller == undefined) {
            return cores[name];
        } else {
            cores[name] = new controller();
        }
    };
    s.module = function(name, controller) {
        /**
         * Если контроллер не передан, то возвращаем ссылку на модуль
         * иначе создаём новый модуль
         */
        if(controller == undefined) {
            return modules[name];
        } else {
            modules[name] = new controller();
        }
    };
    
    /**
     * Обработчик движения пальцев
     */
    s.swipe = function(callback) {
        swipes.push(callback);
    };
    
    /**
     * Заголовок окна
     */
    s.title = function(title) {
        $('#title').text(title);
    };
    
    /**
     * Редирект
     */
    s.go = function(url) {
        window.location.hash = url;
    };
    
    //--- Расширяем возможности приложения за счёт модулей
    s.extension = function(name, controller) {
        s[name] = controller;
    };
    
    /**
     * AJAX-запрос
     * 
     * url: string
     * data: object
     * callback: function
     */
    s.req = function(url, data, callback) {
        data.path = url;
        $.getJSON(s.url, data, function(res) {
            if(!res.err) {
                callback(res.data);
            }
        });
    };

    /**
     * Старт приложения
     */
    s.start = function() {
        if(s.dev()) {
            $(function() { s.init(); });
        } else {
            document.addEventListener("deviceready", s.init, false);
        }
    };

    /**
     * Инициализация основных модулей
     */
    s.init = function() {
        $.each(cores, function(name, module) {
            module.init();
        });
        
        var callSwipe = function(dir) {
            $.each(swipes, function(i, cb) {
                cb(dir);
            });
        };
        $(document).swipe({
            swipeLeft: function() {callSwipe('left');},
            swipeRight: function() {callSwipe('right');}
        });

        $(window).resize(function() {
            $('.cc-navbar').css({width: '100%'});
        });
    };
    
    /**
     * Инициализация дополнительных модулей
     */
    s.initAddon = function() {
        $.each(modules, function(name, module) {
            module.init();
        });
    };

    s.start();
})();
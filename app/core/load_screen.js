app.core('load-screen', function() {
    var s = this;
    var step = 5;
    var timer = null;
    
    var animate = function(pos) {
        pos = (pos != undefined)?((pos > step-1)?0:pos+1):0;
        $('.ls-roller > div').removeClass('active');
        $('.ls-roller > div:eq('+pos+')').addClass('active');
        timer = setTimeout(function() {animate(pos);}, 75);
    };
    
    s.open = function() {
        $('#load-screen').fadeTo(200, 1);
        animate();
    };
    s.close = function() {
        $('#load-screen').fadeTo(100, 0, function() {
            $('#load-screen').hide();
            clearTimeout(timer);
        });
    };
    
    s.init = function() {
        // EJS.config({cache: false});
        var html = new EJS({url: 'files/tmpl/load_screen.ejs'}).render();
        
        $(html).appendTo(document.body);
        for(var i=0; i<step; i++) {
            $('<div></div>').appendTo('.ls-roller');
        }
        $('#load-screen').hide();
        
        app.extension('loadScreen', {open: s.open, close: s.close});
    };
});
app.core('history', function() {
    var s = this;
    
    var route = null;
    var stack = [];
    
    s.back = function() {
        stack.pop(); //--- Delete the history about last page
        
        var obj = (stack.length > 0)?stack[stack.length - 1]:null;
        if(obj) {
            var url = '#'+obj.url;
            if(obj.params) {
                if(typeof obj.params == 'object') {
                    //--- TODO:
                } else {
                    url += '_'+obj.params;
                }
            }
            app.go(url);
        }
    };
    
    s.init = function() {
        route = app.core('route');
        
        route.listen(function(url, p) {
            if(url == 'index') $('#btnBack').hide();
            else $('#btnBack').show();
            var last = (stack.length > 0)?stack[stack.length - 1]:null;
            var obj = {
                url: url,
                params: p
            };
            
            if(!last || last.url != obj.url) stack.push(obj);
        });
        
        $('#btnBack').click(function() {
            s.back();
        });
    };
});
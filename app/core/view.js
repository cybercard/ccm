/**
 * option: {target: string, callback: function}
 */

app.extension('view', function(tpl, data, option) {
    if(option == undefined) option = {};
    var target = (option.target != undefined)?option.target:'content';
    
    if(app.dev()) EJS.config({cache: false});
    var html = new EJS({url: 'files/tmpl/'+tpl+'.ejs'}).render(data);
    
    if(option.output == undefined) {
        $('#'+target).empty();
        $('#'+target).html(html);
        
        if(option.callback != undefined) option.callback();
    } else {
        return html;
    }
});
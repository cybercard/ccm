app.core('user', function() {
    var s = this;
    var title = '';

    var account = {
        aor: {login: '9270517409', pass: '023583'}, // Авраменко Ольга - род
        aau: {login: 'alex', pass: '123'}, // Авраменко Алексей - уч
        vg: {login: '9030215191', pass: '89937'} // ВГ - род
    };
    
    //--- Авторизация
    var auth = function(login, password) {
        var params = {
            cmd: 'user-auth',
            login: login,
            password: password
        };

        if(params.login!='' && params.password!='') {
            app.loadScreen.open();
            //--- Запрашиваем данные пользователя по логину
            app.req('ccm', params, function(res) {
                app.user = res;
                $('div.cc-navbar').css('display', 'block');
                app.core('cache').clean();

                userInit();
                app.initAddon();
                app.socket.connect();
                app.go('index');
            });
        }
    };
    var userInit = function() {
        app.user.is = function(role) {
            return (app.user.roles.indexOf(role)>=0);
        };
    };
    
    s.init = function() {
        //--- Авторизация
        app.route({
            title: 'Авторизация',
            url: 'login',
            templateUrl: 'login',
            controller: function() {
                $('div.cc-navbar').css('display', 'none');
                if(app.dev()) {
                    $('#login').val(account.aor.login);
                    $('#password').val(account.aor.pass);
                }
                $(document).on('click', '#btn-login', function() {
                    auth($('#login').val(), $('#password').val());
                });
            }
        });
        
        app.route({
            title: 'Выход из приложения',
            url: 'logout',
            controller: function() {
                app.user = null;
                app.go('login');
                app.socket.close();
                location.reload(true);
            }
        });
        
        app.extension('user', null);
        app.core('route').listen(function(hash) {
            if(app.user == null && hash != 'login') {
                return {redirect: 'login'};
            }
        });
    };
});
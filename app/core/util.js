var Util = function() {
    var s = this;
    
    s.date = {
        format: function(date) {
            return ([
                s.nn(date.getDate()),
                s.nn(date.getMonth()+1),
                date.getFullYear()
                ]).join('.');
        },
        today: function(date) {
            var td = new Date();
            return (s.date.format(td) === s.date.format(date));
        }
    };
    
    //---- Устарело ----
    s.formatDate = function(date) {
        return s.nn(date.getDate()) + '.' + s.nn((date.getMonth()+1)) + '.' + date.getFullYear();
    };
    s.nn = function(num) {
        num = parseInt(num);
        return (num <= 9)?'0'+num:num;
    }

    s.randString = function(size) {
        return Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, size);
    };
    
    Date.prototype.addDay = function(n) {
        this.setDate(this.getDate() + n);
    };
    Date.prototype.format = function() {
        return s.nn(this.getDate()) + '.' + s.nn((this.getMonth()+1)) + '.' + this.getFullYear();
    };
    Date.prototype.textFormat = function() {
        return s.nn(this.getHours()) + ':' + s.nn(this.getMinutes()) + ' - ' + s.nn(this.getDate()) + '.' + s.nn((this.getMonth()+1)) + '.' + this.getFullYear();
    };
    Date.prototype.clone = function() {
        return new Date(this.getTime());
    };
    Date.prototype.today = function() {
        var td = new Date();
            return (this.format() === td.format());
    };
};
app.extension('util', new Util());
app.core('route', function() {
    var s = this;
    var routes = {};
    var listeners = [];
    
    s.url = null;
    
    /**
     * c = {
     *    url: string > url на который будет откликаться контроллер
     *    templateUrl: string > url шаблона
     *    template: string > (альтернатива) код шаблона
     *    data: function > подготовка данных для шаблона
     *    controller: function > вызывается после рендеринга шаблона
     * }
     */
    var add = function(c) {
        routes[c.url] = c;
    };
    var change = function() {
        var h = window.location.hash;
        if(h.length > 1) {
            var params = '';
            h = h.substr(1);
            
            //--- Проверяем были ли переданы параметры странице
            if(h.indexOf('_') >= 0) {
                var tmp = h.split('_');
                h = tmp[0];
                if(tmp.length == 2) {
                    params = tmp[1];
                } else if(tmp.length > 2) {
                    params = {};
                    for(var i=1; i<tmp.length; i++) {
                        var p = tmp[i].split('-');
                        params[p[0]] = p[1];
                    }
                }
            }
            
            //--- Оповещаем заинтересованные модули в смене страницы
            for(var i in listeners) {
                var res = listeners[i](h, params);
                
                //--- Проверяем результат выполнения слушателей
                if(res != undefined) {
                    if(res.redirect) {
                        app.go(res.redirect);
                        return false;
                    }
                }
            }
            
            //--- Проверка на существование контроллера на переданный url
            if(routes[h] != undefined) {
                s.url = h;
                var r = routes[h];
                var funcs = [];
                
                if(r.headerContent != undefined) {
                    funcs.push(function(cb) {
                        r.headerContent(params, function(tmpl, data) {
                            app.view(tmpl, data, {target: 'headerContent'});
                            var h = $('.cc-navbar').height();
                            $('body').css('padding-top', h + 'px');
                            cb();
                        });
                    });
                } else {
                    $('#headerContent').html('');
                    var h = $('.cc-navbar').height();
                    $('body').css('padding-top', h + 'px');
                }
                
                if(r.data != undefined) {
                    funcs.push(function(cb) {
                        r.data(params, function(data) {
                            app.title(r.title);
                            app.view(r.templateUrl, data, {
                                callback: function() {
                                    r.controller(data);
                                }
                            });
                            cb();
                        });
                    });
                } else {
                    if(!r.templateUrl) {
                        r.controller();
                    } else {
                        app.title(r.title);
                        app.view(r.templateUrl, {}, {
                            callback: function() {
                                if(r.controller) r.controller(params);
                            }});
                    }
                }
                
                new app.async(funcs, function() {});
            } else {
                app.view('not-found');
            }
        }
    };
    
    /**
     * Добавляем слушателя на изменение страницы
     * 
     * controller: function
     */
    s.listen = function(controller) {
        listeners.push(controller);
    };
    s.init = function() {
        app.extension('route', add);
        $(window).on('hashchange', change);
        if(window.location.hash == '') app.go('index');
        else setTimeout(change, 200);
    };
});
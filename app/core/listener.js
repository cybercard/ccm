app.extension('Observe', function(context) {
    context.listeners = {};
    context.variables = {};
    
    context.set = function(variable, value) {
        var map = variable.split('.');
        var last = context.get(variable);
        if(last != value) {
            var change = function(obj, points, val) {
                if(points.length > 0) {
                    var key = points.shift();
                    if(points.length == 0) {
                        obj[key] = val;
                    } else {
                        if(obj[key] == undefined) obj[key] = {};
                        change(obj[key], points, val);
                    }
                } else obj = val;
            };
            change(context.variables, map, value);

            //--- Callbacks
            var ls = context.listeners[variable];
            if(ls) {
                var keys = Object.keys(ls);
                for(var i=0; i<keys.length; i++) {
                    ls[keys[i]](value, last);
                }
            }
        }
    };
    context.get = function(variable) {
        var map = variable.split('.');
        var cursor = function(obj, points) {
            if(points.length > 0 && obj != undefined) {
                obj = obj[points.shift()];
                return cursor(obj, points);
            } else return obj;
        };
        return cursor(context.variables, map);
    };
    
    context.addListener = function(variable, callback) {
        if(context.listeners[variable] == undefined)
            context.listeners[variable] = [];
        context.listeners[variable].push(callback);
    };
});
app.core('cache', function() {
    var s = this;
    var cache = {};
    
    s.clean = function() { cache = {}; };
    
    s.init = function() {
        app.extension('cache', function(key, val, time) {
            if(val == undefined) {
                var res = cache[key];
                if(res != undefined) {
                    if(res.time == 0 || (res.time > (new Date()).getTime())) {
                        return res.val;
                    } else {
                        delete cache[key];
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                time = (time != undefined)?(new Date()).getTime() + time*1000:0;
                cache[key] = {
                    val: val,
                    time: time
                };
            }
        });
    };
});
var Sock = function() {
    var s = this;
    var listners = {};
    var emits = [];
    var socket = null;
    
    s.connect = function() {
        socket = io.connect(app.url);
        
        var keys = Object.keys(listners);
        for(var i=0; i<keys.length; i++) {
            var k = keys[i];
            socket.on(k, listners[k]);
        }
        socket.on('connected', function() {
            for(var i=0; i<emits.length; i++) {
                var e = emits[i];
                socket.emit(e.key, e.data);
            }
        });
        
        socket.on('reboot', function() { location.reload(); });
        socket.on('enter-other-device', function() {
            window.location = '/';
        });
    };
    
    s.on = function(key, callback) {
        listners[key] = callback;
    };
    
    s.emit = function(key, data) {
        if(socket) {
            socket.emit(key, data);
        } else {
            emits.push({key: key, data: data});
        }
    };
    
    s.close = function() {
        if(socket) {
            socket.close();
            socket.disconnect();
            socket = null;
        }
    };
};

app.extension('socket', new Sock());
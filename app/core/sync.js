app.extension('sync', function(functions, callback) {
    var res = null;
    var done = null;
    
    var check = function() {
        var flag = true;
        
        if(typeof functions == 'object') {
            var keys = Object.keys(functions);
            for(var i=0; i<keys.length; i++) {
                var key = keys[i];
                if(done[key] != 1) {
                    flag = false;
                    break;
                }
            }
        } else if(typeof functions == 'array') {
            for(var i=0; i<done.length; i++) {
                if(done[i] != 1) {
                    flag = false;
                    break;
                }
            }
        }
        
        if(flag) callback(res);
    };
    
    var run = function() {
        if(typeof functions == 'object') {
            res = {}, done = {};
                
            var keys = Object.keys(functions);
            for(var i=0; i<keys.length; i++) {
                var key = keys[i];
                res[key] = null;
                done[key] = 0;
                
                (function(k) {
                    functions[k](
                    function(data) {
                            done[k] = 1;
                            res[k] = data;
                            check();
                        }
                    );
                })(key);
            }
        } else if(typeof functions == 'array') {
            res = [], done = [];
            for(var i=0; i<functions.length; i++) {
                res.push(null);
                done.push(0);
                (function(id) {
                    functions[id](
                    function(data) {
                            done[id] = 1;
                            res[id] = data;
                            check();
                        }
                    );
                })(i);
            }
        }
    };
    run();
});

app.extension('async', function(functions, callback) {
    var res = [];
    
    var run = function() {
        if(functions.length > 0) {
            var fnk = functions.shift();
            fnk(function(data) {
                if(data) res.push(data);
                run();
            });
        } else callback(res);
    };
    
    run();
});
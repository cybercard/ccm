app.extension('dictionary', {
    days: [
        {name: 'Воскресенье', short: 'Вс'},
        {name: 'Понедельник', short: 'Пн'},
        {name: 'Вторник', short: 'Вт'},
        {name: 'Среда', short: 'Ср'},
        {name: 'Четверг', short: 'Чт'},
        {name: 'Пятница', short: 'Пт'},
        {name: 'Суббота', short: 'Сб'}
    ]
});
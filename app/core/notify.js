app.core('notify', function() {
    var s = this;
    
    s.beep = function() {};
    s.vibrate = function(options) {
        options = (options != undefined)?options:[200, 50, 300];
        try {
            navigator.notification.vibrate(options);
        } catch (e) {}
    };
    s.status = function(title, message, callback) {
        try {
            cordova.plugins.notification.local.schedule({
                id: 1,
                title: title,
                message: message,
                at: new Date(),
                icon: "file://files/img/logo48.png"
            }, callback);
        } catch (e) {}
    };
    
    s.init = function() {};
});
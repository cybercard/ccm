app.module('visits', function() {
    var s = this;
    var timer = 60; //--- In seconds
    
    var sid = 0;
    var d = {
        prev: new Date(),
        date: new Date(),
        next: new Date()
    };
    var childs = {};
    
    var loadChilds = function(callback) {
        app.module('childs').childs(function(list) {
            for(var i=0; i<list.length; i++) {
                var child = list[i];
                childs['us_'+child.id] = child;
            }
            callback();
        });
    };
    
    s.checkEvents = function(last, sid) {
        var vrb = 'events.us_'+sid;
        for(var i=last.length-1; i>=0; i--) {
            var ev = last[i];
            if(ev.type == 'out' || ev.type == 'in') {
                if(s.get(vrb) != undefined) {
                    if(s.get(vrb).time != ev.time) {
                        app.core('notify').vibrate([100, 50, 50, 50, 100]);
                        var str = ev.time + '. '+app.module('childs').get('child.ch_'+sid).firstName + '. ' + ev.message;
                        app.core('notify').status('Кибер-Карта', str, function() {});
                        s.set(vrb, {time: ev.time, message: ev.message});
                    }
                } else s.set(vrb, {time: ev.time, message: ev.message});
                break;
            }
        }
    };
    
    s.loadEvents = function() {
        var keys = Object.keys(childs);
        if(keys.length > 0) {
            for(var i=0; i<keys.length; i++) {
                var key = keys[i];
                var date = new Date();
                var params = {
                    studentId: childs[key].id,
                    date: date.format(),
                    time: date.getTime()
                };
                app.req('student/attendance', params, function(res) {
                    s.checkEvents(res, params.studentId);
                });
            }
        }
        setTimeout(function() { s.loadEvents(); }, timer * 1000);
    };
    
    var changeDate = function(direction) {
        var day = d.date.getDay();
        var days = (direction > 0)?((day == 6)?2:1):((day == 1)?-2:-1);
        d.date.addDay(days);
        showVisits(d.date);
    };
    
    var getVisits = function(date, callback) {
        var textDate = date.format() + ' ('+app.dictionary.days[date.getDay()].name+')';
        var params = {
            studentId: sid,
            date: d.date.format(),
            nocache: (new Date()).getTime()
        };
        
        var cacheKey = 'visit-'+sid+'-'+d.date.format();
        var cache = app.cache(cacheKey);
        if(!cache) {
            app.req('student/attendance', params, function(res) {
                var out = {visits: res};
                app.cache(cacheKey, out, 2*60);
                callback(out);
            });
        } else callback(cache); 
        
        $('#visit-date').text(textDate);
    };
    
    var showVisits = function(date) {
        var child = app.module('childs').getChildBySid(sid);

        if(child.tarif.id != 2440) {
            getVisits(date, function(visits) {
                app.view('visit-content-day', visits, {target: 'visit-content'});
            });
        } else {
            app.view('visit-access-denied', null, {target: 'visit-content'});
        }
    };
    
    s.init = function() {
        new app.Observe(this);
        app.module('childs').addItemMenu('Посещения', 'childs-visits_{sid}', 'transfer');
        if(app.user.is('parent')) loadChilds(s.loadEvents);
        
        $(document).on('click', '#visit-btn-prev', function() { changeDate(0); });
        $(document).on('click', '#visit-btn-next', function() { changeDate(1); });
        
        app.route({
            title: 'Посещения',
            url: 'childs-visits',
            templateUrl: 'visit-content',
            headerContent: function(data, callback) {
                var sid = 0;
                sid = (typeof data == 'object')?data.sid:data;
                var child = app.module('childs').get('child.ch_'+sid);
                callback('visit-header', {child: child, vd: {date: d.date.format() + ' ('+app.dictionary.days[d.date.getDay()].name+')'}});
            },
            data: function(data, callback) {
                sid = parseInt(data);
                d.date = new Date();
                
                callback({});
            },
            controller: function() {
                showVisits(d.date);
                
                app.loadScreen.close();
            }
        });
    };
});
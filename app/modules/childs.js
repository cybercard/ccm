app.module('childs', function() {
    var s = this;
    var menu = [];
    
    new app.Observe(s);

    //--- Загружаем данные о ребёнке с сервера
    s.loadChild = function(sid, callback) {
        var visit = app.module('visits');
        
        //--- Синхронно загружаем необходимые данные о ребёнке
        new app.sync({
                tarif: function(cb) { tarif(sid, cb); },
                balance: function(cb) { balance(sid, cb); },
                student: function(cb) { app.req('student', {studentId: sid}, cb); }
            },
        function(data) {
            var res = data.student;
            res.balance = data.balance;
            res.tarif = data.tarif;
            res.event = visit.get('events.us_'+sid);
            s.set('child.ch_'+sid, res);

            visit.addListener('events.us_'+sid, function(val) {
                s.set('child.ch_'+sid+'.event', val);
            });
            callback(res);
        });
    };
    
    //--- Баланс по питанию
    var balance = function(id, callback) {
        app.req('student/balance/ext', {studentId: id}, function(res) {
            callback(res);
        });
    };
    
    //--- Тариф
    var tarif = function(id, callback) {
        app.req('student/tariff', {studentId: parseInt(id)}, function(res) {
            callback({id: res.id, name: res.name, price: res.price});
        });
    };
    
    //--- Добавить пункт меню к профилю ребёнка
    s.addItemMenu = function(title, url, icon, condition) {
        menu.push({
            title: title,
            url: url,
            icon: icon,
            condition: condition
        });
    };
    
    //--- Список пунктов меню
    s.getMenuItems = function(sid) {
        var res = [];
        var std = s.get('child.ch_'+sid);

        for(var i=0; i<menu.length; i++) {
            var m = menu[i];
            if(m.condition != undefined) {
                if(m.condition(std)) res.push(m);
            } else res.push(m);
        }
        return res;
    };
    
    //--- Возвращает список детей
    s.childs = function(callback) {
        var list = s.get('child');
        if(!list) {
            app.req('parent', {parentId: app.user.uid}, function(parent) {
                var list = parent.children;
                var fnc = [];
                for(var i=0; i<list.length; i++) {
                    var child = list[i];
                    (function(sid) {
                        fnc.push(function(cb) {
                            var res = s.get('child.ch_'+sid);
                            if(!res) {
                                s.loadChild(sid, function(res) { cb(res); });
                            } else cb(res);
                        });
                    })(child.id);
                }

                new app.sync(fnc, function(list) {
                    var res = [];
                    var keys = Object.keys(list);
                    for(var i=0; i<keys.length; i++) res.push(list[keys[i]]);
                    callback(res);
                });
            });
        } else {
            var res = [];
            var keys = Object.keys(list);
            for(var i=0; i<keys.length; i++) 
                res.push(list[keys[i]]);
            callback(res);
        }
    };

    s.getChildBySid = function(sid) {
        return s.get('child.ch_'+sid);
    };

    //--- Инициализация модуля
    s.init = function() {
        menu = [];
        if(app.user.is('parent')) {
            s.addItemMenu('Аккаунт', 'childs-account_{sid}', 'credit-card');
        }
        
        app.route({
            title: 'Аккаунт ученика',
            url: 'childs-account',
            templateUrl: 'child-account',
            headerContent: function(data, callback) {
                var child = app.module('childs').get('child.ch_'+data);
                callback('profile-header-child', child);
            },
            data: function(data, callback) {
                app.req('ccm', {cmd: 'user-account', uid: data}, function(res) {
                    callback({account: res, uid: data});
                });
            },
            controller: function(data) {
                $('#studentAccountBtn').click(function() {
                    var params = {};
                    
                    if(typeof data == 'object' && data.account != undefined) {
                        params = {
                            cmd: 'user-save-pass',
                            id: data.account._id,
                            pass: $('#accountPass').val()
                        };
                    } else {
                        params = {
                            cmd: 'user-create-child',
                            user: {
                                uid: parseInt(data.uid),
                                parent: app.user.uid,
                                login: $('#accountName').val(),
                                pass: $('#accountPass').val(),
                                roles: ['student']
                            }
                        };
                    }                    
                    app.req('ccm', params, function(res) {
                        app.core('history').back();
                    });
                });
            }
        });
    };
});
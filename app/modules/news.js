app.module('news', function() {
    var s = this;
    var limit = 10;
    
    /**
     * Загрузка последних новостей
     */
    var load = function(callback) {
        var cache = app.cache('news');
        //var cache = false;
        if(cache) {
            callback(cache);
        } else {
            app.loadScreen.open();
            var params = {
                cmd: 'news',
                limit: limit,
                accountId: app.user.uid
            };
            app.req('ccm', params, function(res) {
                if(res) {
//                    res = (res.slice(limit*-1)).reverse();
                    app.cache('news', res);
                    callback(res);
                } else callback([]);
            });
        }
    };
    
    /**
     * Инициализация модуля
     */
    s.init = function() {
        //--- Контроллер главной страницы приложения
        app.route({
            title: 'События',
            url: 'news',
            templateUrl: 'news',
            data: function(data, callback) {
                load(function(res) {
                    callback({news: res});
                });
            },
            controller: function() {
                app.loadScreen.close();
            }
        });
        
        //--- Страница выбранной новости
        app.route({
            title: 'Подробности',
            url:'news-get',
            templateUrl: 'news-page',
            data: function(data, callback) {
                var id = parseInt(data);
                var news = app.cache('news');
                callback({item: news[id]});
            },
            controller: function() {
                app.loadScreen.close();
            }
        });

        $(document).on('click', '.news > div', function() {
            app.go('#news-get_' + $(this).data('nid'));
        });
        app.module('menu').addMenuItem('События', 'news', 'bell', 0, 2);
    };
});
app.module('profile', function() {
    var s = this;
    new app.Observe(s);
    var listenersFlag = false;
    
    var loadData = function(callback) {
        if(app.user.is('parent')) {
            app.req('parent', {parentId: app.user.uid}, function(parent) {
                var res = {
                    uid: app.user.uid,
                    avatar: app.user.avatar,
                    accountNumber: app.user.accountNumber,
                    debt: app.user.debt,
                    login: app.user.login,
                    name: [app.user.lastName, app.user.firstName, app.user.middleName].join(' '),
                    lastName: app.user.lastName,
                    firstName: app.user.firstName,
                    middleName: app.user.middleName,
                    address: parent.address,
                    phone: parent.phone
                };
                s.set('data', res);
                callback(res);
            });
        } else { callback(app.user); }
    };
    
    var parentPage = function(data, callback) {
        app.module('childs').childs(function(list) {
            if(!listenersFlag) {
                listenersFlag = true;
                var mc = app.module('childs');
                for(var i=0; i<list.length; i++) {
                    var child = list[i];
                    var variable = 'child.ch_'+child.id+'.'
                    mc.addListener(variable+'event', function(val) {
                        $('#profile-child-'+child.id+' .profile-event b').text(val.time + ' - ' + val.message);
                    });
                }
            }
            callback({
                childs: list,
                menu: app.module('childs').getMenuItems
            });
        });
    };
    
    var studentPage = function(data, callback) {
        app.module('lesson').lessons(app.user.uid, callback);
    };
    
    s.init = function() {
        app.module('menu').addMenuItem('Профиль', 'index', 'user');
        app.module('menu').addMenuItem('Выход', 'logout', 'log-out', 0, 100);
        $(document).on('click', '.profile-menu .btn', function() {
            app.go('#' + $(this).data('url'));
        });
        
        //--- Главная страница
        app.route({
            url: 'index',
            title: 'Профиль',
            templateUrl: 'profile-content',
            headerContent: function(dt, callback) {
                if(app.user.is('parent')) {
                    var data = s.get('data');
                    if(!data) {
                        loadData(function(data) {
                            callback('profile-header', data);
                        });
                    } else callback('profile-header', data);
                } else if(app.user.is('student')) {
                    var md = app.module('childs');
                    var profile = md.get('child.ch_' + app.user.uid);
                    if(!profile) {
                        md.loadChild(app.user.uid, function(profile) {
                            callback('profile-header-child', profile);
                        });
                    } else {
                        callback('profile-header-child', profile);
                    }
                }
            },
            data: function(data, callback) {
                if(app.user.is('parent')) {
                    parentPage(data, callback);
                } else if(app.user.is('student')) {
                    studentPage(data, callback);
                }
            },
            controller: function() {
                app.loadScreen.close();
            }
        });
    };
});
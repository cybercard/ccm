app.module('tarif', function() {
    var s = this;
    
    s.init = function() {
        app.module('menu').addMenuItem('Тарифы', 'tarif', 'random', 0, 3);
        
        app.route({
            title: 'Тарифы',
            url: 'tarif',
            templateUrl: 'tarifs',
            data: function(data, callback) {
                var cache = app.cache('tarifs');
                if(!cache) {
                    app.loadScreen.open();
                    app.req('ccm', {cmd: 'tarifs'}, function(res) {
                        var dt = {tarifs: res};
                        app.cache('tarifs', dt);
                        callback(dt);
                    });
                } else callback(cache);
            },
            controller: function() {
                app.loadScreen.close();
            }
        });

         //app.route({
         //    title: 'Тарифы',
         //    url: 'tarif',
         //    templateUrl: 'tarifs',
         //    data: function(data, callback) {
         //        callback();
         //    },
         //    controller: function() {}
         //});
         //app.route({
         //    title: 'Тариф',
         //    url: 'tarif-student',
         //    templateUrl: 'tarif',
         //    data: function(id, callback) {
         //        app.module('load-screen').open();
         //        var tarif = app.cache('tarif_'+id);
         //        if(tarif) {
         //            callback({
         //                studentId: id,
         //                tarif: tarif,
         //                tarifs: app.cache('tarifs_'+id)
         //            });
         //        } else {
         //            app.req('student/tariff', {studentId: parseInt(id)}, function(data) {
         //                app.cache('tarif_'+id, data);
         //                app.req('student/tariffs', {studentId: parseInt(id)}, function(tarifs) {
         //                    app.cache('tarifs_'+id, tarifs);
         //                    callback({tarif: data, studentId: id, tarifs: tarifs});
         //                });
         //            });
         //        }
         //    },
         //    controller: function() {
         //        app.module('load-screen').close();
         //    }
         //});
    };
});
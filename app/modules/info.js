app.module('info', function() {
    var s = this;
    
    s.init = function() {
        app.module('menu').addMenuItem('Информация', 'info', 'info-sign', 0, 4);
        
        app.swipe(function(dir) {
            if(dir == 'right' && app.core('route').url == 'info') {
                app.go('index');
            }
        });
        
        // app.module('index').addLiunk('Информация', 'info', 'info-sign');
        
        app.route({
            title: 'Информация',
            url: 'info',
            templateUrl: 'info'
        });
    };
});
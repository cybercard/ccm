app.module('lesson', function() {
    var s = this;
    var days = app.dictionary.days;
    
    s.lessons = function(sid, callback) {
        var dateToString = app.util.formatDate(new Date());
        var params = {
            studentId: parseInt(sid),
            fromDate: dateToString,
            toDate: dateToString
        };
        
        var ckey = 'lesson-'+sid+'-'+dateToString;
        var cache = app.cache(ckey);
        if(cache) {
            callback({days: cache});
        } else {
            app.req('student/schedule', params, function(res) {
                app.cache(ckey, res);
                callback({days: res});
            });
        }
    };
    
    s.init = function() {
        app.module('childs').addItemMenu('Расписание', 'childs-lessons_{sid}', 'calendar');
        
        app.route({
            title: 'Расписание',
            url: 'childs-lessons',
            templateUrl:'lesson',
            headerContent: function(data, callback) {
                var child = app.module('childs').get('child.ch_'+data);
                callback('profile-header-child', child);
            },
            data: function(sid, callback) {
                app.core('load-screen').open();
                s.lessons(sid, callback);
            },
            controller: function() {
                app.core('load-screen').close();
            }
        });
    };
});
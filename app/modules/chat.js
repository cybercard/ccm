app.module('chat', function() {
    var s = this,
        room = null,
        rooms = {},
        messages = {};
        menuIndex = 0;
        
    var resize = function() {
        var h = $(window).height() - $('.cc-navbar').height() - $('.chat-form').height();
        $('#message-box').css('height', (h)+'px');
    };
    
    var selectRoom = function(gid) {
        if(gid != room) {
            room = gid;
            if($('#'+gid).length == 0) {
                $('<div id="'+gid+'"></div>').appendTo('#message-box');
                if(messages[gid] != undefined) {
                    insertMessages(room, messages[gid]);
                } else loadHistory(room);
            }
            
            $('#message-box > div').hide();
            $('#'+room).show();
            
            $('.chat-rooms > div').removeClass('active');
            $('.chat-rooms > .room-'+room).addClass('active');
            scroll();
            $('.room-'+room + ' .chat-room-notify').addClass('unvisible');
            rooms[room].notify = false;
        }
    };
    var loadHistory = function(gid, lastId, callback) {
        if(isOpenedRoom(gid)) app.loadScreen.open();
        app.req('ccm', {cmd: 'chat-messages', gid: gid, lastId: lastId}, function(list) {
            messages[gid] = [];
            for(var i=0; i<list.length; i++) {
                messages[gid].push(list[i]);
            }
            insertMessages(gid, messages[gid]);
            app.loadScreen.close();
            if(callback) callback();
        });
    };
    var pushMessage = function() {
        var data = {group: room, text: $('#chatText').val()};
        if(data.group && data.text != '') {
            app.socket.emit('chat-send', data);
            $('#chatText').val('');
        } else {
            console.log('Ошибка при отправки сообщения', data);
        }
    };
    var pullMessage = function(data) {
        var gid = data.group;
        if(messages[gid] == undefined) {
            loadHistory(gid);
        } else {
            messages[gid].push(data);
            if(isOpenedRoom(gid)) {
                insertMessages(gid, [data]);
            }
        }
        if(!isOpenedChat() && data.uid != app.user.uid) {
            app.core('notify').vibrate([50, 100, 50]);
            app.module('menu').setEvent(menuIndex);
        } else if(!isOpenedRoom(gid)) notify(gid);
        
    };
    var notify = function(gid) {
        $('.room-'+gid + ' .chat-room-notify').removeClass('unvisible');
        rooms[gid].notify = true;
    };
    var insertMessages = function(gid, collection) {
        $(app.view('chat-message', {messages: collection}, {output: true})).appendTo('#'+gid);
        if(isOpenedRoom(gid)) {
            scroll(600);
        }
    };
    
    var scroll = function(time) {
        $('#message-box').animate({
            scrollTop: $('#'+room).height()
        }, time||0);
    };
    
    var setDefaultsRooms = function() {
        if(app.user.is("parent") || app.user.is('student')) {
            var key = 'fm_'+(app.user.parent||app.user.uid);
            rooms[key] = {title: 'Семья', ico: 'home', gid: key};
        }
        if(app.user.is("teacher") || app.user.is("student")) {
            if(app.user.school != undefined) {
                var key = 'sc_'+app.user.school;
                rooms[key] = {title: 'Школа', ico: 'blackboard', gid: key};
            }
            if(app.user.class != undefined) {
                var key = 'cl_'+app.user.class;
                rooms[key] = {title: 'Класс', ico: 'user', gid: key};
            }
        }
    };

    var isOpenedRoom = function(gid) {
        if(isOpenedChat() && room == gid) {
            return true;
        } else return false;
    };
    var isOpenedChat = function() {
        return (app.core('route').url == 'chat');
    };
    
    s.init = function() {
        menuIndex = app.module('menu').addMenuItem('Сообщения', 'chat', 'envelope', 0, 1);
        
        setDefaultsRooms();
        
        app.route({
            title: 'Сообщения',
            url: 'chat',
            templateUrl: 'chat',
            headerContent: function(data, callback) { callback('chat-header', {rooms: rooms}); },
            data: function(data, callback) { callback({rooms: rooms}); },
            controller: function() {
                resize();
                var keys = Object.keys(rooms);
                try {
                    room = null;
                    selectRoom(rooms[keys[0]].gid);
                } catch(e) {
                    console.log('Ошибка выбора комнаты поумолчанию', rooms);
                }
            }
        });
        
        app.socket.on('new-message', pullMessage);
        app.socket.emit('chat-connect', {uid: app.user.uid});
        
        $(document).on('click', '.chat-rooms > div', function() {
            selectRoom($(this).data('gid'));
        });
        $(document).on('click', '#btnChatSend', pushMessage);
        $(window).resize(resize);
    };
});
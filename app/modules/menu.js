app.module('menu', function() {
    var s = this;
    var opened = false;
    var items = [];
    
    var compare = function(a, b) {
        if(a.weight > b.weight) return 1;
        if(a.weight < b.weight) return -1;
        else return 0;
    };
    
    var open = function() {
        if(opened == false) {
            app.view('menu-main', {items: items}, {target: 'menuBox'});
            var headerHeight = $('.cc-navbar > div:eq(0)').height();
            var docHeight = $(document).height();

            $('#menuBlackScreen')
                .css('display', 'block')
                .css('top', headerHeight + 'px')
                .css('height', (docHeight - headerHeight) + 'px')
                .fadeTo('fast', 1);
            $('#menuBox')
                .css('top', headerHeight + 'px')
                .css('height', (docHeight - headerHeight) + 'px');
            $('#btnMenu > span').removeClass('glyphicon-menu-hamburger').addClass('glyphicon-arrow-left');
            $('#menuBox').animate({width: 'toggle'});
            $('#btnSubMenu').hide();
            $('#btnMenu > div').css('display', 'none');
        }
        opened = true;
    };
    
    var close = function() {
        if(opened == true) {
            $('#menuBlackScreen').fadeTo('fast', 0, function() {
                $('#menuBlackScreen').css('display', 'none');    
            });
            $('#btnMenu > span').removeClass('glyphicon-arrow-left').addClass('glyphicon-menu-hamburger');
            $('#btnSubMenu').show();
            $('#menuBox').animate({width: 'toggle'});
        }
        opened = false;
    };

    var getItemId = function(key) {
        for(var i=0; i<items.length; i++) {
            if(items[i].key == key) {
                return i;
            }
        }
    };

    s.addMenuItem = function(title, link, icon, events, weight) {
        var key = app.util.randString(5);
        items.push({
            key: key,
            title: title,
            link: link,
            icon: icon,
            event: (events != undefined)?events:0,
            weight: (weight != undefined)?weight:0
        });
        items.sort(compare);

        return key;
    };
    
    s.toggle = function() {
        if(opened) {
            close();
        } else open();
    };

    s.setEvent = function(menuItemKey) {
        var menuItemId = getItemId(menuItemKey);
        var cnt = ++items[menuItemId].event;
        
        if(opened) {
            cnt = (cnt>99)?'+99':cnt;
            $('.menu-item:eq('+menuItemId+') .badge').text(cnt);
        } else {
            $('#btnMenu > div').css('display', 'block');
        }
    };
    
    s.init = function() {
        $(document).on('click', '#btnMenu', s.toggle);
        
        app.swipe(function(direction) {
            if(direction == 'right') {
                open();
            } else if(direction == 'left') close();
        });
        
        $(function() {
            $('<div id="menuBlackScreen"></div>').appendTo('body');
            $('<div id="menuBox"></div>').appendTo('body');
            $('#menuBlackScreen, #menuBox').hide();
        });
        
        app.core('route').listen(function() {
            if(opened) close();
        });
        
        $(document).on('click', '.menu-item', function() {
            var id = getItemId($(this).data('key'));
            items[id].event = 0;
            app.go('#' + items[id].link);
        });
    };
});